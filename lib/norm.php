<?php

if (file_exists(ROOT . DS . 'vendor' . DS . 'NotORM.php')) {
    require_once (ROOT . DS . 'vendor' . DS . 'NotORM.php');
}

class Norm {

    private $_db;
    private $_pdo;
    private $_driver;
    private $_table;
    private $_debug;

    public function connect() {
        $this->_pdo = new PDO(DB_DSN, DB_USER, DB_PASSWORD, [
            PDO::ATTR_PERSISTENT => true, // conexión persistente
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);
        $this->_db = new NotORM($this->_pdo);
        $this->_db->debug = $this->_debug;
        $this->_driver = $this->_pdo->getAttribute(PDO::ATTR_DRIVER_NAME);
    }

    public function __construct($tableName, $debugMode = false) {
        $this->_table = $tableName;
        $this->_debug = $debugMode;
        $this->connect();
    }

    public function getStructure() {
        $describe = array(
            'sqlite' => 'PRAGMA table_info (' . $this->_table . ')',
            'mysql' => 'DESCRIBE ' . $this->_table
        );
        $pk = array(
            'sqlite' => 'pk',
            'mysql' => 'Key'
        );

        $field = array(
            'sqlite' => 'name',
            'mysql' => 'Field'
        );

        $statement = $this->_pdo->query($describe[$this->_driver]);
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        $returned = [];
        foreach ($result as $item) {
            $returned[] = array(
                'Field' => $item[$field[$this->_driver]],
                'Key' => $item[$pk[$this->_driver]]
            );
        }
        return $returned;
    }

    public function all() {
        return $this->_db->{$this->_table}();
    }

    public function first($condition, $value) {
        return $this->_db->{$this->_table}($condition, $value)->fetch();
    }

    public function get(int $id) {
        return $this->_db->{$this->_table}[$id];
    }

    public function add($data) {
        return $this->_db->{$this->_table}->insert($data);
    }
    
    public function exec($sql)
    {
        return $this->_pdo->exec($sql);
    }
    
    public function allBySql($sql)
    {
        return $this->_pdo->query($sql);
    }
    
    function __destruct() {
        
    }

}
