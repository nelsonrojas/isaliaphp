<?php

class Controller
{

    protected $_module;

    protected $_controller;

    protected $_action;

    protected $_template = '';

    protected $variables = array();

    protected $_scaffold = null;
    

    public function __construct($controller, $action)
    {
        Security::injectAntiCSRFHeader();
        $this->_controller = $controller;
        $this->_action = $action;
    }

    /**
     * Set Variables *
     */
    public function set($name, $value)
    {
        $this->variables[$name] = $value;
    }

    public function __destruct()
    {
        $this->render();
    }

    private function render()
    {
        $this->set('controller_name', $this->_controller);
        $this->set('action_name', $this->_action);
        $this->set('scaffold_name', $this->_scaffold);

        extract($this->variables);
        $view_route = ROOT . DS . 'app' . DS . 'views' . DS;
        $template_file = $view_route . '_shared' . DS . 'layouts' . DS . 'default.phtml';
        if ($this->_template != null) {
            $template_file = $view_route . '_shared' . DS . 'layouts' . DS . $this->_template . '.phtml';
        }

        ob_start('ob_gzhandler');

        $seeError = true;
        if (file_exists($view_route . $this->_controller . DS . $this->_action . '.phtml')) {
            include ($view_route . $this->_controller . DS . $this->_action . '.phtml');
            $seeError = false;
        } else {
            if ($this->_scaffold != null) {
                if (file_exists($view_route . '_shared' . DS . 'scaffold' . DS . $this->_action . '.phtml')) {
                    include ($view_route . '_shared' . DS . 'scaffold' . DS . $this->_action . '.phtml');
                    $seeError = false;
                }
            }
        }

        if ($seeError == true) {
            include ($view_route . '_shared' . DS . 'layouts' . DS . 'error.phtml');
        }

        View::setContent(ob_get_clean());

        if ($this->_template === null) {
            echo View::getContent();
        } else {
            include ($template_file);
        }
    }
}
