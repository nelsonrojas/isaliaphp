<?php

class AdminController extends Controller
{

    public function before_filter()
    {
        if (! Auth::isLogged()) {
            return Router::to('sesion/login');
        }
    }
}
