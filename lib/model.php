<?php
/**
 * Model
 * @abstract
 * Permite realizar operaciones de bases de dato usando Norm como base
 */
class Model extends Norm {
    protected $_model;
    
    /**
     * constructor de la clase
     * establece el valor de las variables _model y _table para ser usados 
     * luego en Norm
     */
    function __construct() {
        $this->_model = get_class($this);
        $this->_table = strtolower($this->_model);
        parent::__construct($this->_table, DEVELOPMENT_ENVIRONMENT == true);
    }
    
    function __destruct() {
        parent::__destruct();
    }
}
