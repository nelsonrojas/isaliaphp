<?php

/**
 * Based on https://www.w3schools.com/php/php_file_upload.asp
 */
class Upload
{

    protected $_message = '';

    protected $_upload_dir = '';

    protected $_currentFile = '';

    public function __construct()
    {
        $this->_upload_dir = UPLOAD_DIR;
    }

    private function getRandom($ext)
    {
        return md5(rand(1, 100) . time()) . '.' . $ext;
    }

    public function getCurrentFile()
    {
        return $this->_currentFile;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    private function isValid($element, $target_file, $imageFileType)
    {
        $result = true;

        // Check image file is a actual image or fake image
        $check = getimagesize($_FILES[$element]["tmp_name"]);
        if ($check !== false) {
            $this->_message = "File is an image - " . $check["mime"] . ".";
        } else {
            $this->_message = "File is not an image.";
            $result = false;
        }

        // Check file already exists
        if (file_exists($target_file)) {
            $this->_message = "Sorry, file already exists.";
            $result = false;
        }
        // Check file size
        if ($_FILES[$element]["size"] > 1000000) {
            $this->_message = "Sorry, your file is too large. " . $_FILES[$element]["size"];
            $result = false;
        }
        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $this->_message = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $result = false;
        }

        return $result;
    }

    public function add($element)
    {
        $target_dir = $this->_upload_dir;
        $target_file = $target_dir . basename($_FILES[$element]["name"]);
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

        $file_name = $this->getRandom($imageFileType);
        $target_file = $target_dir . $file_name;

        $uploadOk = 1;
        // Check $uploadOk is set to 0 by an error
        if ($this->isValid($element, $target_file, $imageFileType) === false) {
            $this->_message = "Sorry, your file was not uploaded.";
            $uploadOk = 0;
            // everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES[$element]["tmp_name"], $target_file)) {
                $this->_message = "The file " . basename($_FILES[$element]["name"]) . " has been uploaded.";
                $this->_currentFile = $target_file;
            } else {
                $this->_message = "Sorry, there was an error uploading your file." . error_get_last()['message'];
            }
        }
        if ($this->_message != '') {
            Logger::debug($this->_message);
        }
        return $uploadOk;
    }
}
