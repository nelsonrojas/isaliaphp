<?php

/**
 * 
 * @author nelson
 * class Tag
 * Helper para cargar Css y Js
 */
class Tag
{

    public static function css($file)
    {
        return '<link href="' . PUBLIC_PATH . 'css' . DS . $file . '.css" rel="stylesheet">';
    }

    public static function js($file)
    {
        return '<script type="text/javascript" src="' . PUBLIC_PATH . 'js' . DS . $file . '.js"></script>';
    }

    public static function resolve($source)
    {
        return '/' . $source;
    }
}
