<?php

/**
 * 
 * @author nelson
 * class Auth
 * Clase para la gestión de autenticación de los usuarios
 */
class Auth
{

    private $tableName = '';

    private $loginField = '';

    private $passField = '';

    static $nameSpace = 'auth_ns';

    /**
     * __construct
     * @abstract
     * constructor de la clase en el que se debe establecer el nombre de la tabla, el campo que
     * sera comparado como login, y el campo que correspondera revisar como contraseña
     * Se asume que la contraseña esta basada en BCRYPT con costo de 12
     * 
     * @param string $tableName
     * @param string $loginField
     * @param string $passField
     */
    public function __construct($tableName, $loginField, $passField)
    {
        $this->tableName = $tableName;
        $this->loginField = $loginField;
        $this->passField = $passField;
    }

    /**
     * set
     * @abstract
     * permite cargar el contenido de un array asociativo como variables de la sesion auth_ns
     * el contenido del array puede ser accedido usando la funcion get
     * 
     * @param array $record
     * @return void
     */
    public function set($record)
    {
        $_SESSION[static::$nameSpace]['logged'] = hash('md5', date('Y-m-d H:n:i') . PROJECT_NAME . rand(100, 999));
        foreach ($record as $key => $value) :
            if (strpos($key, '_in') === FALSE && strpos($key, '_at') === FALSE && strpos($key, $this->passField) !== 0) :
                $_SESSION[static::$nameSpace][$key] = Security::sanitize($value);
			endif;

        endforeach
        ;
    }

    /**
     * destroy
     * @abstract
     * destriye el contenido de los elementos dentro de la sesion auth_ns
     * 
     * @return boolean
     */
    public static function destroy()
    {
        unset($_SESSION[static::$nameSpace]['logged']);
        foreach ($_SESSION[static::$nameSpace] as $key => $value) :
            unset($_SESSION[static::$nameSpace][$key]);
        endforeach
        ;
        return TRUE;
    }

    /**
     * get
     * @abstract
     * permite extraer elementos que estan contenidos en la variable de la sesion auth_ns
     * 
     * @param string $key
     */
    public static function get($key)
    {
        if (isset($_SESSION[static::$nameSpace][$key])) :
            return $_SESSION[static::$nameSpace][$key];
        else :
            return NULL;
        endif;
    }

    /**
     * isLogged
     * @abstract
     * Permite reconocer si la sesion se ha iniciado o no
     * 
     * @return boolean
     */
    public static function isLogged()
    {
        return (isset($_SESSION[static::$nameSpace]['logged']) === TRUE);
    }

    /**
     * check
     * @abstract
     * realizar la comprobacion de la sesion segun la configuracion y los datos enviados
     * 
     * @param string $login
     * @param string $password
     * @return boolean
     */
    public function check($login = null, $password = null)
    {
        $login = Security::sanitize($login);
        $password = Security::sanitize($password);
        $user = (new Norm($this->tableName))
                ->first($this->loginField . ' = ?', $login);
        
        Logger::debug(implode(' ', $user));
        
        if ($user && password_verify($password, $user[$this->passField])) {
            $this->set($user, static::$nameSpace);
            return true;
        } else {
            return false;
        }
    }
}
