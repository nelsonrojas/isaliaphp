<?php
/**
 * class View
 * @abstract
 * permite cargar el contenido que debera renderizarse
 * 
 */
class View
{

    /**
     *
     * @var $_yield
     * @abstract
     * almacena temporalmente el contenido que debera enviarse al cliente
     */
    static $_yield;

    /**
     * partial
     * @abstract
     * carga el contenido de una vista parcial
     * @param string $partial
     * @param array $variables
     */
    public static function partial($partial, $variables = null)
    {
        if (is_array($variables) && $variables !== null) {
            extract($variables);
        }

        $view_route = ROOT . DS . 'app' . DS . 'views' . DS;

        if (file_exists($view_route . '_shared' . DS . 'partials' . DS . $partial . '.phtml')) {
            include ($view_route . '_shared' . DS . 'partials' . DS . $partial . '.phtml');
        } else {
            include ($view_route . '_shared' . DS . 'layouts' . DS . 'error.phtml');
        }
    }

    /**
     * setContent
     * @param document $content
     * @abstract
     * carga el contenido en la variable de paso _yield
     */
    public static function setContent($content)
    {
        static::$_yield = $content;
    }

    
    /**
     * getContent
     * @abstract
     * extrae el contenido de la variable de paso _yield
     * @return document
     */
    public static function getContent()
    {
        return static::$_yield;
    }
}
