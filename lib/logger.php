<?php

class Logger
{
    
    private static $tmpDir = ROOT . DS . 'app' . DS . 'tmp' . DS . 'log';
    
    public static function debug($message = '')
    {
        static::write('debug', $message);
    }
    
    public static function error($message = '')
    {
        static::write('error', $message);
    }

    public static function write($type = 'debug', $message = '')
    {
        try
        {
            $arch = fopen(realpath(static::$tmpDir) . DS . "log_" . date("Y-m-d") . ".txt", "a+");

            fwrite($arch, "[" . date("Y-m-d H:i:s.u") . " " . $_SERVER['REMOTE_ADDR'] . " " . " - $type ] " . $message . "\n");
            fclose($arch);
        } catch (Exception $ex) {
            throw (new Exception('Imposible escribir en el directorio ' . static::$tmpDir));
        }
        
    }
}
