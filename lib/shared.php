<?php

function stripSlashesDeep($value) {
    if (is_array($value)) {
        $value = array_map('stripSlashesDeep', $value);
    } else {
        $value = stripslashes($value);
    }
    return $value;
}

/**
 * Revisa la existencia de Magic Quotes las remueve *
 */
function removeMagicQuotes() {
    if (get_magic_quotes_gpc()) {
        $_GET = stripSlashesDeep($_GET);
        $_POST = stripSlashesDeep($_POST);
        $_COOKIE = stripSlashesDeep($_COOKIE);
    }
}

/**
 * Revisa register globals y los remueve *
 */
function unregisterGlobals() {
    if (ini_get('register_globals')) {
        $array = array(
            '_SESSION',
            '_POST',
            '_GET',
            '_COOKIE',
            '_REQUEST',
            '_SERVER',
            '_ENV',
            '_FILES'
        );
        foreach ($array as $value) {
            foreach ($GLOBALS[$value] as $key => $var) {
                if ($var === $GLOBALS[$key]) {
                    unset($GLOBALS[$key]);
                }
            }
        }
    }
}

/**
 * @see 
 * https://stackoverflow.com/questions/1993721/how-to-convert-camelcase-to-camel-case
 * @param string $input
 * @return string
 */
function from_camel_case($input) {
  preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
  $ret = $matches[0];
  foreach ($ret as &$match) {
    $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
  }
  return implode('_', $ret);
}

/**
 * Funcion principal *
 */
function callHook() {
    global $url;

    $urlArray = (array) explode("/", $url);

    if (isset($urlArray[0]) && !empty($urlArray[0])) {
        if (is_dir(ROOT . DS . 'app' . DS . 'controllers' . DS . $urlArray[0])) {
            array_shift($urlArray);
        }
    }

    $controller = (isset($urlArray[0]) && !empty($urlArray[0])) ? $urlArray[0] : DEFAULT_CONTROLLER;
    array_shift($urlArray);
    $action = (isset($urlArray[0]) && !empty($urlArray[0])) ? $urlArray[0] : DEFAULT_ACTION;
    array_shift($urlArray);
    $queryString = $urlArray;

    $controllerName = $controller;
    $controller = ucwords($controller);
    $controller .= 'Controller';
    $dispatch = new $controller($controllerName, $action);

    // incluye el callback before_filter en caso de existir
    if ((int) method_exists($controller, 'before_filter')) {
        call_user_func(array(
            $dispatch,
            'before_filter'
        ));
    }

    // procede con la accion solicitada
    if ((int) method_exists($controller, $action)) {
        call_user_func_array(array(
            $dispatch,
            $action
                ), $queryString);
    } else {
        /* Enviar error */
        throw(new Exception('Vaya, parece que algo no va bien. Metodo no encontrado en Controller ' . ucwords($controllerName)));
    }
}

/**
 * Autocarga de cualquier clase que sea requerida *
 */
spl_autoload_register(
        function($className) {
    $className = from_camel_case($className);
    if (file_exists(ROOT . DS . 'lib' . DS . $className . '.php')) {
        require_once (ROOT . DS . 'lib' . DS . $className . '.php');
    } else if (file_exists(ROOT . DS . 'app' . DS . 'lib' . DS . $className . '.php')) {
        require_once (ROOT . DS . 'app' . DS . 'lib' . DS . $className . '.php');
    } else if (file_exists(ROOT . DS . 'app' . DS . 'controllers' . DS . $className . '.php')) {
        require_once (ROOT . DS . 'app' . DS . 'controllers' . DS . $className . '.php');
    } else if (file_exists(ROOT . DS . 'app' . DS . 'models' . DS . $className . '.php')) {
        require_once (ROOT . DS . 'app' . DS . 'models' . DS . $className . '.php');
    } else {
        /* Enviar error */
        throw(new Exception('Vaya, parece que algo no va bien. Imposible cargar ' . $className ));
    }
});

/**
 * sanitizar los elementos que pudieran haberse ingresado
 */
removeMagicQuotes();
unregisterGlobals();

/**
 * llamar a la funcion principal / despachador
 */
callHook();
