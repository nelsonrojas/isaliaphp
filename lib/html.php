<?php

/**
 * 
 * @author nelson
 * class Html
 * Helper para generar diferentes elementos html
 */
class Html
{

    public static function link($to, $text, $attributes = '')
    {
        return "<a href='" . PUBLIC_PATH . "$to' $attributes>$text</a>";
    }

    public static function img($url, $alt = '', $attributes = '')
    {
        return "<img src='/" . PUBLIC_PATH . "img/$url' alt='$alt' $attributes />";
    }
}
