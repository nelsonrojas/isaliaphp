<?php

/**
 * Router
 * @abstract
 * Se encarga de hacer las redirecciones solicitadas
 */
class Router
{

    /**
     * to
     * @abstract
     * realizar la redireccion
     * @param string $route
     */
    public static function to($route)
    {
        header('Location: ' . PUBLIC_PATH . $route, TRUE, 302);
    }
}
