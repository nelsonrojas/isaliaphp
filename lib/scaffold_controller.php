<?php

class ScaffoldController extends AdminController
{

    public function before_filter()
    {
        parent::before_filter();
        if (Input::hasPost($this->_scaffold) && ! Input::isSafe()) {
            if ($this->_action != 'del') {
                Logger::error('UNSAFE', $this->_controller . DS . $this->_action);
            }
        }
    }

    public function index()
    {
        $this->set('data', (new Norm($this->_scaffold))->all());
    }

    public function add()
    {
        if (Input::hasPost($this->_scaffold)) {
            if ((new Norm($this->_scaffold))->add(Input::post($this->_scaffold))) {
                (new Flash())->set('success', 'Agregado exitosamente!');
                Router::to($this->_controller);
            }
        }
    }

    public function edit(int $id)
    {
        $data = (new Norm($this->_scaffold))->get($id);
        $this->set('data', $data);

        if (Input::hasPost($this->_scaffold)) {
            if ($data->update(Input::post($this->_scaffold)) == 1) {
                (new Flash())->set('success', 'Actualizado exitosamente!');
                Router::to($this->_controller);
            } else {
                (new Flash())->set('danger', 'Imposible actualizar!');
            }
        }
    }

    public function del(int $id)
    {
        $data = (new Norm($this->_scaffold))->get($id);
        if ($data && $data->delete()) {
            (new Flash())->set('success', 'Eliminado exitosamente!');
        }
        Router::to($this->_controller);
    }
}