-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `enlace`;
CREATE TABLE `enlace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(300) NOT NULL,
  `grupo` varchar(80) NOT NULL,
  `url` varchar(1000) NOT NULL,
  `icono` varchar(40) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '0',
  `creado_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `actualizado_in` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `enlace` (`id`, `titulo`, `grupo`, `url`, `icono`, `activo`, `creado_at`, `actualizado_in`) VALUES
(1,	'Panoramas',	'CONTENIDO',	'/',	'',	1,	'2017-12-16 13:16:26',	'0000-00-00 00:00:00'),
(2,	'Comercio',	'CONTENIDO',	'comercio',	'',	1,	'2017-12-16 13:16:42',	'0000-00-00 00:00:00'),
(3,	'Municipalidad de Linares',	'EXTERNO',	'http://www.munilinares.cl',	'',	1,	'2017-12-16 13:17:09',	'0000-00-00 00:00:00'),
(4,	'Panoramas',	'ADMIN',	'admin/panoramas',	'',	1,	'2017-12-17 20:45:49',	'0000-00-00 00:00:00'),
(5,	'Comercios',	'ADMIN',	'admin/comercios',	'',	1,	'2017-12-17 20:45:55',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `imagen`;
CREATE TABLE `imagen` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(80) NOT NULL,
  `url_miniatura` varchar(80) DEFAULT '',
  `aprobada` int(11) NOT NULL DEFAULT '0',
  `orden` int(11) DEFAULT '0',
  `item_id` bigint(20) NOT NULL,
  `creado_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `imagen` (`id`, `url`, `url_miniatura`, `aprobada`, `orden`, `item_id`, `creado_at`) VALUES
(3,	'a7e86e90caa5f6ae22a189ecdae9873a.jpg',	NULL,	1,	NULL,	1,	'2017-12-26 14:49:17'),
(4,	'88f1944bec32649982b415ac213535ba.jpg',	NULL,	1,	NULL,	1,	'2017-12-26 14:52:05'),
(5,	'cc3233ce9fca39a3f0e77fd22585506a.jpg',	NULL,	1,	NULL,	1,	'2017-12-26 14:55:43'),
(6,	'e8d31c440969162317280b2366897a9e.jpg',	NULL,	1,	NULL,	2,	'2017-12-27 00:48:04'),
(7,	'7bb0324095e2bc1959fb9e35eb38ea33.jpg',	NULL,	1,	NULL,	3,	'2017-12-27 08:17:46'),
(8,	'a7e3c0efee7566408ef1a4f160282967.jpg',	NULL,	1,	NULL,	4,	'2017-12-27 08:22:19'),
(9,	'fcd4615242c4d1a2a33c8b60a51c1e16.jpg',	NULL,	1,	NULL,	5,	'2017-12-27 08:24:10');

DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(1000) NOT NULL,
  `cuerpo` text NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_termino` date DEFAULT NULL,
  `tags` varchar(1000) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `tipo_item_id` int(11) NOT NULL,
  `eliminado` int(11) DEFAULT '0',
  `aprobado` int(11) DEFAULT '0',
  `creado_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `actualizado_in` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `lat` varchar(80) DEFAULT NULL,
  `lng` varchar(80) DEFAULT NULL,
  `direccion` varchar(300) DEFAULT NULL,
  `localidad_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `item` (`id`, `titulo`, `cuerpo`, `fecha_inicio`, `fecha_termino`, `tags`, `usuario_id`, `tipo_item_id`, `eliminado`, `aprobado`, `creado_at`, `actualizado_in`, `lat`, `lng`, `direccion`, `localidad_id`) VALUES
(1,	'panorama de ejemplo',	'panorama de ejemplo',	'2017-12-26',	'2017-12-31',	NULL,	1,	1,	0,	1,	'2017-12-27 03:09:09',	'2017-12-26 23:14:27',	NULL,	NULL,	NULL,	NULL),
(2,	'Comercio de ejemplo',	'Comercio de ejemplo',	'1970-01-01',	'1970-01-01',	NULL,	1,	2,	1,	1,	'2017-12-27 00:43:32',	'2017-12-27 07:06:17',	NULL,	NULL,	'Comercio de ejemplo',	1),
(3,	'otro panorama de ejemplo',	'otro panorama de ejemplo\r\notro panorama de ejemplo\r\notro panorama de ejemplootro panorama de ejemplo\r\notro panorama de ejemplo\r\notro panorama de ejemplo\r\notro panorama de ejemplo',	'2017-12-27',	'2017-12-31',	NULL,	1,	1,	0,	1,	'2017-12-27 04:18:19',	'2017-12-27 08:18:06',	NULL,	NULL,	NULL,	NULL),
(4,	'tercer panorama de la semana',	'tercer panorama de la semana\r\ntercer panorama de la semana\r\ntercer panorama de la semana\r\ntercer panorama de la semana\r\ntercer panorama de la semana\r\ntercer panorama de la semana',	'2017-12-25',	'2017-12-31',	NULL,	1,	1,	0,	1,	'2017-12-27 08:22:06',	'2017-12-27 08:22:56',	NULL,	NULL,	NULL,	NULL),
(5,	'cuarto panorama fuera de la línea base',	'cuarto panorama fuera de la línea base\r\ncuarto panorama fuera de la línea base\r\ncuarto panorama fuera de la línea base',	'2017-12-26',	'2017-12-29',	NULL,	1,	1,	0,	1,	'2017-12-27 08:23:38',	'2017-12-27 08:24:23',	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `localidad`;
CREATE TABLE `localidad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `activa` int(11) NOT NULL DEFAULT '0',
  `creada_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `actualizada_in` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `localidad` (`id`, `nombre`, `activa`, `creada_at`, `actualizada_in`) VALUES
(1,	'Linares',	1,	'2017-12-17 20:09:43',	'0000-00-00 00:00:00'),
(2,	'San Javier',	1,	'2017-12-17 20:11:05',	'0000-00-00 00:00:00'),
(3,	'Parral',	1,	'2017-12-17 20:11:14',	'0000-00-00 00:00:00'),
(4,	'Villa Alegre',	1,	'2017-12-17 20:11:49',	'0000-00-00 00:00:00'),
(5,	'Longaví',	1,	'2017-12-17 20:11:48',	'0000-00-00 00:00:00'),
(6,	'Colbún',	1,	'2017-12-17 20:11:47',	'0000-00-00 00:00:00'),
(7,	'Retiro',	1,	'2017-12-17 20:11:46',	'0000-00-00 00:00:00'),
(8,	'Yerbas Buenas',	1,	'2017-12-17 20:12:02',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `tiempo`;
CREATE TABLE `tiempo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `minima` varchar(20) NOT NULL,
  `maxima` varchar(20) NOT NULL,
  `texto` varchar(80) NOT NULL,
  `creado_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tipo_item`;
CREATE TABLE `tipo_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `tag` varchar(100) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1',
  `creado_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `actualizado_in` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tipo_item` (`id`, `nombre`, `tag`, `activo`, `creado_at`, `actualizado_in`) VALUES
(1,	'Panoramas',	'PANORAMA',	1,	'2017-12-15 17:44:20',	'0000-00-00 00:00:00'),
(2,	'Comercio',	'COMERCIO',	1,	'2017-12-15 17:44:29',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(80) NOT NULL,
  `creado_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `actualizado_in` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `usuario` (`id`, `username`, `email`, `password`, `creado_at`, `actualizado_in`) VALUES
(1,	'Nelson',	'nelson.rojas.n@gmail.com',	'$2y$12$jkBzS9q1i/YlzAU1KvItqOu8/4LvbGkMEvL7j0A249WbOP/ZMH6fa',	'2018-01-16 23:42:21',	'0000-00-00 00:00:00');

-- 2018-01-17 11:05:54
