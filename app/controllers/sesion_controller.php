<?php

class SesionController extends Controller
{

    function login()
    {
        $this->_template = 'sesion';

        if (Input::hasPost('access2')) {
            $this->set('access2_login', Input::post('access2')['login']);
            
            Logger::debug(implode(' ', Input::post('access2')));

            $auth = new Auth('User', 'login', 'password');

            if ($auth->check(Input::post('access2')['login'], Input::post('access2')['pass'])) {
                Flash::valid('Bienvenido ' . Auth::get('name'));
                return Router::to('');
            } else {
                Flash::error('Imposible conectar!');
            }
        }
    }

    function logout()
    {
        Auth::destroy();
        Flash::info('Hasta la próxima :)');
        return Router::to('');
    }
}
