<?php

class IndexController extends Controller
{

    function index()
    {
        $this->set('tags', (new Orm('tags'))->all()->order('tag_name'));
    }
}
