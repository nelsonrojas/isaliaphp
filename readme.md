# IsaliaPHP
## Framework PHP
### [![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/nelsonrojas/isaliaPHP/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/nelsonrojas/isaliaPHP/?branch=master)

Hola, bienvenida/bienvenido!
He realizado este proyecto luego de haber usado algunos frameworks, y de haber leído un artículo llamado 'write your own php mvc framework', o, en su traducción 'escribe tu propio framework mvc con php'. Pueden revisar los enlaces a continuación.

http://anantgarg.com/2009/03/13/write-your-own-php-mvc-framework-part-1/

http://anantgarg.com/2009/03/30/write-your-own-php-mvc-framework-part-2/

**El propósito principal de este repositorio es para ser usado como material de estudio, y no hay garatías explícitas en caso que usted decida usarlo como herramienta de productividad**

He seleccionado [[NotORM](http://www.notorm.com/)](http://www.notorm.com/) como gestor de datos, principalmente por estar basado en PDO. NotORM está incluído como vendor, y lo he utilizado para reemplazar el SQLBuilder que originalmente 
I've selected [[NotORM](http://www.notorm.com/)](http://www.notorm.com/) as data layer (because it is based on PDO). NotORM is included as a vendor, and I've decided to use it in replace of the original SQLBuilder.
I use it with the Model class.

The CSS library included is [[Skeleton](http://getskeleton.com/)](http://getskeleton.com/) (2.0.4), and Javascript library is jQuery (1.9.1)

I've provided a database script to explain how to create a simple CRUD (please see categories controller), and the authentication process (in the index controller, user model, and auth class).
The database script is in the config/data.sql file.
You need to change the config/config.php file to set your database credentials.

The authentication is based on BCRYPT.

If you have any comment or suggestion, I'll be very glad about your comments.

Best regards

Nelson.-

