<?php

define('ROOT', dirname(dirname(__FILE__)));
define('DS', DIRECTORY_SEPARATOR);
define('APP_PATH', ROOT . DS . 'app' . DS);
define('LIB_PATH', ROOT . DS . 'lib' . DS);

/** Check if environment is development and display errors * */
define('DEVELOPMENT_ENVIRONMENT', TRUE);

if (DEVELOPMENT_ENVIRONMENT === TRUE) {
    error_reporting(E_ALL ^ E_STRICT);
    ini_set('display_errors', 'On');
} else {
    error_reporting(E_ALL);
    ini_set('display_errors', 'Off');
    ini_set('log_errors', 'On');
    ini_set('error_log', ROOT . DS . 'tmp' . DS . 'logs' . DS . 'error.log');
}

define('START_TIME', microtime(TRUE));

/** You need to change PUBLIC_PATH with your own site path * */
define('PUBLIC_PATH', DS . basename(dirname(__DIR__)) . DS);

$url = isset($_GET['url']) ? $_GET['url'] : '/';

session_start();

require_once (LIB_PATH . 'bootstrap.php');
