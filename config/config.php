<?php

/** MySQL Server settings **/
define('DB_DSN', 'mysql:host=127.0.0.1;dbname=natures_v3;charset=utf8');
define('DB_USER', 'root');
define('DB_PASSWORD', '');

/**
 * Sqlite3 settings *
 */

/*
 * define('DB_DSN', 'sqlite:' . APP_PATH . 'data' . DS . 'mydb.sqlite3');
 * define('DB_USER', '');
 * define('DB_PASSWORD', '');
 *
 */

/**
 * App Settings *
 */
define('SAFETY_SEED', 'A.01sT-cZf32-arP11-zoR3n-S4f3');
define('DEFAULT_CONTROLLER', 'index');
define('DEFAULT_ACTION', 'index');
define('UPLOAD_DIR', 'img' . DS . 'uploads' . DS);
define('PROJECT_NAME', basename(dirname(__DIR__)));
